/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#include "QiTalkAPI.h"
#include <QDebug>

QiTalkAPI::QiTalkAPI(QObject *parent) : QObject(parent)
{
    //connect(&serial, &QiTalkSerial::read, this, &QiTalkAPI::handleRead);
    serial.setPortName("/dev/ttyS0");
    serial.setBaudRate(QSerialPort::Baud115200);
    connect(&serial, &QSerialPort::readyRead, this, &QiTalkAPI::handleReadyRead);
}

QiTalkAPI::~QiTalkAPI()
{
    serial.close();
}

bool QiTalkAPI::open()
{
    if (!serial.open(QIODevice::ReadWrite)) {
        qWarning() << "QiTalkSerial: Failed opening serial:" << serial.errorString();
        return false;
    }

    if (request("AT").last() != "OK") {
        qWarning() << "QiTalk Test Command failed!";
        return false;
    }

    const QVector<QString> config_commands = {
        "AT+CLIP=1",
        "AT+CMGF=1",
        "AT+QURCCFG=\"urcport\",\"uart1\"",
        "ATS0=0",
        "AT+CCFC=0,4",
        "AT+CMGD=1,4"
    };

    for (int i = 0, sz = config_commands.size(); i < sz; ++i) {
        if (request(config_commands[i]).last() != "OK") {
            qWarning() << "QiTalk: failed to execute" << config_commands[i];
        }
    }

    qDebug() << "QiTalk is Initialized";
    return true;
}

QVector<QString> QiTalkAPI::readAll(const bool is_sms)
{
    QByteArray read_utf8 = serial.readAll();
    /* Make sure ALL the data comes through */

    if (is_sms) {
        while (serial.waitForReadyRead(50)) {
            read_utf8 += serial.readAll();
        }
    } else {
        while (serial.waitForReadyRead(50) || !QString::fromUtf8(read_utf8).endsWith(term)) {
            read_utf8 += serial.readAll();
        }
    }
    QVector<QString> read_data = QString::fromUtf8(read_utf8).split(term).toVector();
    if (read_data.first().isEmpty()) {
        read_data.removeFirst();
    }
    if (read_data.last().isEmpty()) {
        read_data.removeLast();
    }
    return read_data;
}


void QiTalkAPI::write(const QString write_data)
{
    const QByteArray write_utf8 = QString(write_data + term).toUtf8();
    const qint64 write_bytes = serial.write(write_utf8);
    if (write_bytes != write_utf8.size()) {
        qWarning() << "Warn: not all data sent!";
    }
    serial.flush();
}

QVector<QString> QiTalkAPI::request(const QString write_data, const bool is_sms)
{
    expect_response = true;
    write(write_data);

    if (!serial.waitForReadyRead(350)) {
        qWarning() << "TIMEOUT";
        expect_response = false;
        return QVector<QString>({"TIMEOUT"});
    }

    const QVector<QString> read_data = readAll(is_sms);
    expect_response = false;
    return read_data;
}

QVector<QString> QiTalkAPI::sendSMS(const QString num, const QString body)
{
    qDebug() << endl << "Sending to" << num << ":" << body;

    if (request(tr("AT+CMGS=\"%1\"").arg(num), true).last() != "> ") {
        qFatal("Error sending text. Please restart by pressing the physical switch on your PiTalk");
    }

    const QVector<QString> sms_response = request(body + sms_term);
    if (sms_response.last() != "OK") {
        qWarning() << "Failed to send text to" << num << ":" << body;
    }
    return sms_response;
}

void QiTalkAPI::handleReadyRead()
{
    if (expect_response) return;
    const QVector<QString> read_data = readAll();
    qDebug() << "From QiTalkSerial: Read: " << read_data;

    for (int i = 0, sz = read_data.size(); i < sz; ++i) {
        qDebug() << "QiTalkAPI::handleReadyRead()[" << i << "]:" << read_data[i];
    }

    /* Is it an incoming SMS? */
    QRegularExpressionMatch sms_re = regex.new_sms.match(read_data.first());
    if (sms_re.hasMatch()) {
        qDebug() << "New SMS incoming";

        const QString sms_memory = sms_re.captured(1);
        const QString sms_index  = sms_re.captured(2);

        qDebug() << "SMS Memory:" << sms_memory;
        qDebug() << "SMS Index:"  << sms_index;

        /* Get the SMS data */
        const QVector<QString> sms_data = request(tr("AT+CMGR=%1").arg(sms_index));

        if (sms_data.last() == "OK") {

            qDebug() << "Printing SMS data";
            for (int i = 0, sz = sms_data.size(); i < sz; ++i) {
                qDebug() << tr("SMS data[%1]: %2").arg(i).arg(sms_data[i]);
            }

            /* Get the full text body */
            QString sms_body = "";
            for (int i = 2, sz = sms_data.size() - 1; i < sz; ++i) {
                sms_body += sms_data[i] + "\n";
            }

            /* Get the metadata*/
            const QVector<QString> sms_metadata = sms_data[1].split("\"").toVector();

            for (int i = 0, sz = sms_metadata.size(); i < sz; ++i) {
                qDebug() << "Metadata" << i << ":" << sms_metadata[i];
            }

            /* Get the phone number */
            const QString sms_num = sms_metadata[3];

            /* Get the date and time of the message */
            const QDateTime sms_datetime = QDateTime::fromString(
                QRegularExpression("^(\\d\\d/\\d\\d/\\d\\d,\\d\\d:\\d\\d:\\d\\d).*").match(sms_metadata[5]).captured(1),
                "yy/MM/dd,hh:mm:ss"
            );

            /* Delete the SMS from the modem's memory*/
            if (request(tr("AT+CMGD=%1").arg(sms_index)).last() != "OK") {
                qWarning() << "Error deleting message from modem's internal storage";
            }
            emit smsReceived(sms_num, sms_body, sms_datetime);
        } else {
            qWarning() << "Error getting SMS data";
        }
    } else {
        emit read(read_data);
    }
}
