/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#include "QiTalkReplFrontend.h"
#include <QDebug>

QiTalkReplFrontend::QiTalkReplFrontend(const QString phone_num, QWidget *parent): QWidget(parent)
{
    lay = new QVBoxLayout(this);
    setLayout(lay);

    sms_line_edit = new QLineEdit(this);
    line_edit = new QLineEdit(this);
    list_widget = new QListWidget(this);

    lay->addWidget(list_widget);
    lay->addWidget(line_edit);
    lay->addWidget(sms_line_edit);

    connect(line_edit, &QLineEdit::returnPressed, this, &QiTalkReplFrontend::sendSMS);
    connect(&serial, &QiTalkAPI::read, this, &QiTalkReplFrontend::notif);
    connect(&serial, &QiTalkAPI::smsReceived, this, &QiTalkReplFrontend::smsReceived);

    //connect(sms_line_edit, &QLineEdit::editingFinished, this, &QiTalkReplFrontend::sendSMS);

    serial.open();

    this->phone_num = phone_num;
}

QiTalkReplFrontend::~QiTalkReplFrontend()
{

    list_widget->clear();
    lay->removeWidget(list_widget);
    lay->removeWidget(line_edit);

    delete list_widget;
    delete sms_line_edit;
    delete line_edit;
    delete lay;
}

void QiTalkReplFrontend::write()
{
    const QString text = line_edit->text();
    new QListWidgetItem("Request: " + text, list_widget);
    QVector<QString> response = serial.request(text);
    for (int i = 0, sz = response.size(); i < sz; ++i) {
        qDebug() << "Response to " << text << "[" << i << "]:" << response[i];
        new QListWidgetItem("Response: " + response[i], list_widget);
    }
    line_edit->clear();
}

void QiTalkReplFrontend::sendSMS()
{
    const QString text = line_edit->text();
    const QString phone_num = sms_line_edit->text();
    QString indicator = "SMS SEND";
    if (serial.sendSMS(phone_num, text).last() != "OK") {
        indicator = "FAILED TO SEND";
    }
    new QListWidgetItem("SMS SEND: " + phone_num + ": " + text, list_widget);
    line_edit->clear();
}

void QiTalkReplFrontend::notif(QVector<QString> text)
{
    for (int i = 0, sz = text.size(); i < sz; ++i) {
        qDebug() << "Notification " << "[" << i << "]:" << text[i];
        new QListWidgetItem("Notification: " + text[i], list_widget);
    }
}

void QiTalkReplFrontend::smsReceived(const QString num, const QString body, const QDateTime datetime)
{
    new QListWidgetItem(tr("RX SMS: %1 (%2): %3").arg(num).arg(datetime.toString("hh:mm:ss")).arg(body), list_widget);
}
