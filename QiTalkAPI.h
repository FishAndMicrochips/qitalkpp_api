#ifndef QITALKAPI_H
#define QITALKAPI_H

#include <QObject>
#include <QRegularExpression>
#include <QDateTime>
#include <QSerialPort>
//#include "QiTalkSerial.h"

class QiTalkAPI : public QObject
{
    Q_OBJECT
public:
    explicit QiTalkAPI(QObject *parent = nullptr);
    ~QiTalkAPI();

signals:
    void read(const QVector<QString> read_data);
    void smsReceived(const QString num, const QString body, const QDateTime datetime);
public slots:
    bool open();
    void write(const QString write_data);
    QVector<QString> request(const QString write_data, const bool is_sms = false);

    QVector<QString> sendSMS(const QString num, const QString body);
private:
    //QiTalkSerial serial;
    QSerialPort serial;
    bool expect_response = false;
    QVector<QString> readAll(const bool is_sms = false);

    // Key Regexps to compare to
    // TODO: Add regexps for * SMS indication * SMS details * Sending SMS
    /*
    # parsing new sms indication
    CMTI_REGEX = re.compile(r'^\+CMTI:\s*"([^"]+)",(\d+).*$')
    # parsing new sms details
    CMGR_SMS_REGEX = re.compile(r'^\+CMGR: "([^"]+)","([^"]+)",[^,]*,"([^"]+)".*$')
    */
    struct {
        QRegularExpression error       = QRegularExpression("^\\+(CM[ES]) ERROR: (\\d+)$");
        QRegularExpression new_sms     = QRegularExpression("^\\+CMTI:\\s*\"([^\"]+)\",(\\d+).*$");
        QRegularExpression sms_details = QRegularExpression("^\\+CMGR: \"([^\"]+)\",\"([^\"]+)\",[^,]*,\"([^\"]+)\".*$");
    } regex;

    const QString term = "\r\n";
    const QString sms_term = "\u001A";

private slots:
    //void handleRead(const QVector<QString> read_data);
    void handleReadyRead();
};

#endif // QITALKAPI_H
