#ifndef QITALKREPLFRONTEND_H
#define QITALKREPLFRONTEND_H

#include <QWidget>
#include <QListWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QVector>
#include <QString>
#include <QDateTime>

#include "QiTalkAPI.h"

class QiTalkReplFrontend : public QWidget
{
    Q_OBJECT
public:
    explicit QiTalkReplFrontend(const QString phone_num, QWidget *parent = nullptr);
    ~QiTalkReplFrontend();
signals:

public slots:

private:
    QListWidget *list_widget;
    QLineEdit *line_edit;
    QLineEdit *sms_line_edit;
    QVBoxLayout *lay;

    QiTalkAPI serial;

    QString phone_num = "";

signals:
    void sigWrite(const QString data);

private slots:
    void write();
    void notif(const QVector<QString> text);
    void smsReceived(const QString num, const QString body, const QDateTime datetime);
    void sendSMS();
    //void response(const QString request, const QString response);
};

#endif // QITALKREPLFRONTEND_H
