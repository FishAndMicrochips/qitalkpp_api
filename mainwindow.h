#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QiTalkReplFrontend.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QString phone_num, QWidget *parent = nullptr);
    ~MainWindow();

private:
    QiTalkReplFrontend *qitalk_repl_frontend;
};

#endif // MAINWINDOW_H
