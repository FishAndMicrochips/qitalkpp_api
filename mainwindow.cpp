/* QiTalk: A QT-Based Chat Application for the Quectel-UC15
 * Copyright (C) 2020 Amber Gingell
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
#include "mainwindow.h"

MainWindow::MainWindow(const QString phone_num, QWidget *parent): QMainWindow(parent)
{
    qitalk_repl_frontend = new QiTalkReplFrontend(phone_num, this);
    setCentralWidget(qitalk_repl_frontend);
}

MainWindow::~MainWindow()
{
    delete qitalk_repl_frontend;
}
